* _Links in site title on home page_
* _Optimise home page hover animations for touch_
* Over-large gap under photo on home page
* _'More...' buttons on home page panels_
* Consider horizontal nav floating on scroll
* Navigation for xs screens
* Footer
* _Association logos_