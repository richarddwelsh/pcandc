var animDelay = 150;
var pageName;
var navigationBar, navBarTop;
var fadeOutDue = false;

$(document).ready(function() {
    pageName = $("body").attr("data-page-name");
    $("#nav-" + pageName).addClass("active");

    $(".banner-image").css("background-image", "url(" + $(".banner-image img").attr("src") + ")");

    // behaviours for home page

    $(".nested-a").on("mouseenter", function() {
        var childA = $(this), parentA = $(this).closest("a");

        childA.prop("originalHref", parentA.attr("href"));
        parentA[0].href = childA.attr("data-href");
        console.log(childA.attr("data-href")); console.log(parentA.attr("href"));
    })

    $(".nested-a").on("mouseleave", function() {
        var childA = $(this), parentA = $(this).closest("a");

        parentA.attr("href", childA.prop("originalHref"));
    })

    $(".hotspot").on("touchstart", function() {
        this.touch = true; // this event happens first only on touch screens
    })

    $(".hotspot").on("mouseenter", function() {
        if (this.touch) {
            // this event is happening as part of a touchscreen tap, so don't do a hover animation
            this.touch = false;
        }
        else {
            // this is a 'classic' mouse hover event
            fadeOutDue = false; // cancel a fadeout of the overlay if it is due
//             console.log("IN");
            $(this).css("z-index", 200);
            $(this).animate({"top": -10}, {duration: animDelay + 50});
            $("#overlay").fadeIn(animDelay);
        }
    })

    $(".hotspot").on("mouseleave", function() {
//         console.log("OUT");
        $(this).css("z-index", "auto");
        $(this).animate({"top": 0}, {
            duration: animDelay + 50, 
            complete: function() {
                
            }
        });
        fadeOutDue = true;
        window.setTimeout(fadeOutOverlay, 100);
    })

    if (window.innerWidth <= 350) {
        $(".navigation").draggable({axis: "x", containment: [-300, 0, 300, 100]});
      }

//     navigationBar = $(".navigation");
//     navBarTop = navigationBar.offset().top;

    // $(window).scroll(function () {
    //     if ($(this).scrollTop() > navBarTop)
    //         navigationBar.addClass("scrolled");
    //     else
    //         navigationBar.removeClass("scrolled");
    // })
})

function fadeOutOverlay() {
    if (fadeOutDue)
        $("#overlay").fadeOut(animDelay);
}